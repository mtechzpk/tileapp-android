package com.mtechsoft.tileapp.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.mtechsoft.tileapp.MainActivity;
import com.mtechsoft.tileapp.R;

public class LandingActivity extends AppCompatActivity {
    public NavController navController;
    RelativeLayout rlToolbar;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        rlToolbar = findViewById(R.id.rlToolbar);
        initNavigation();


    }

    private void initNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    if (destination.getLabel().equals("fragment_login") || destination.getLabel().equals("WelcomFragment")) {
                        rlToolbar.setVisibility(View.GONE);
                    } else {
                        rlToolbar.setVisibility(View.VISIBLE);

                    }
//                    rlToolbar.setVisibility(View.VISIBLE);
//                    tvTitle.setText(destination.getLabel());
                }

            }
        });
    }

}
