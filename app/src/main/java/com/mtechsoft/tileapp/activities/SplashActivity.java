package com.mtechsoft.tileapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.mtechsoft.tileapp.R;

public class SplashActivity extends AppCompatActivity {
Button bCreate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initViews();
        onClicks();
    }

    private void onClicks() {
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SplashActivity.this,LandingActivity.class));
            }
        });
    }

    private void initViews() {
        bCreate=findViewById(R.id.bCreate);
    }

}
