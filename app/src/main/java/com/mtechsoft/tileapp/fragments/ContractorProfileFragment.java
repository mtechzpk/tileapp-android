package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtechsoft.tileapp.Models.ImageSliderModel;
import com.mtechsoft.tileapp.Models.VideoSliderModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ImageSlider1Adapter;
import com.mtechsoft.tileapp.adapters.ImageSliderAdapter;
import com.mtechsoft.tileapp.adapters.VideoSliderAdapter;

import java.util.ArrayList;
import java.util.List;

public class ContractorProfileFragment extends Fragment {

    public ContractorProfileFragment() {
        // Required empty public constructor
    }

    private View v;
    private Button see_all, ver_perfil;
    ImageView profile_img, back;
    private RecyclerView recyclerView, recyclerView1;
    private ImageSlider1Adapter pAdapter;
    private VideoSliderAdapter mAdapter;
    private List<VideoSliderModel> videoSliderModels;
    private List<ImageSliderModel> imageSliderModels;
    private LinearLayoutManager horizontalLayoutManager;
    TextView tv_friends, email, web_address, phone, exp_text;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_contractor_profile, container, false);

        init();

        return v;
    }

    private void init() {
        see_all = v.findViewById(R.id.see_all);
//        ver_perfil = v.findViewById(R.id.ver_perfil);
        profile_img = v.findViewById(R.id.profile_img);
        back = v.findViewById(R.id.back);
        tv_friends = v.findViewById(R.id.tv_friends);
        email = v.findViewById(R.id.email);
        web_address = v.findViewById(R.id.web_address);
        phone = v.findViewById(R.id.phone);
        exp_text = v.findViewById(R.id.exp_text);

        recyclerView = v.findViewById(R.id.image_slider);
        recyclerView1 = v.findViewById(R.id.image_slider1);

        setPoemsName();
        pAdapter = new ImageSlider1Adapter(getActivity(), imageSliderModels);
        horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setAdapter(pAdapter);

        setImages();
        mAdapter = new VideoSliderAdapter(getActivity(), videoSliderModels);
        horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView1.setLayoutManager(horizontalLayoutManager);
        recyclerView1.setAdapter(mAdapter);

        see_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new AllProfileFragment())
                        .addToBackStack(null).commit();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new ResultadoPeopleFragment())
                        .addToBackStack(null).commit();
            }
        });
    }

    private void setPoemsName() {
        imageSliderModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ImageSliderModel imageSliderModel = new ImageSliderModel();
            imageSliderModels.add(imageSliderModel);

        }
    }

    private void setImages() {
        videoSliderModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            VideoSliderModel videoSliderModel = new VideoSliderModel();
            videoSliderModels.add(videoSliderModel);

        }
    }
}
