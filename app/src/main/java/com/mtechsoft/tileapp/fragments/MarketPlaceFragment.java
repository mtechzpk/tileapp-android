package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mtechsoft.tileapp.Models.MarketModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.MarketAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MarketPlaceFragment extends Fragment {

    public MarketPlaceFragment() {
        // Required empty public constructor
    }

    private View v;
    private EditText search_et;
    private ImageView search_ic;
    private RecyclerView recyclerView;
    private MarketAdapter pAdapter;
    private List<MarketModel> marketModelList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_market_place, container, false);

        init();
        return v;
    }

    private void init() {
        recyclerView = v.findViewById(R.id.market_rv);
        search_et = v.findViewById(R.id.search_et);
        search_ic = v.findViewById(R.id.search_ic);

        setModelItems();

        pAdapter = new MarketAdapter(getActivity(), marketModelList);
        pAdapter.setOnItemClickListener(new MarketAdapter.OnItemClickListener() {
            @Override
            public void onItemclick(int position) {

                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new CivilRecyclerDetailFragment()).addToBackStack(null).commit();
//                Toast.makeText(getActivity(), "Go to Next", Toast.LENGTH_SHORT).show();
// ye ho gya h click lg gay h test kr agr issue ata h to dekhty hain
            }
        });


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);



    }

    private void setModelItems() {
        marketModelList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            MarketModel marketModel = new MarketModel();
            marketModelList.add(marketModel);
        }
    }
}
