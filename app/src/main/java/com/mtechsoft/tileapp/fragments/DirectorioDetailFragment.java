package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mtechsoft.tileapp.Models.DirectorioDetailModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.DirectrioDetailsAdapter;
import com.mtechsoft.tileapp.adapters.MarketAdapter;

import java.util.ArrayList;
import java.util.List;

public class DirectorioDetailFragment extends Fragment {
    private View v;
    private RecyclerView recyclerView;
    private ImageView back;
    private DirectrioDetailsAdapter pAdapter;
    private List<DirectorioDetailModel> directorioDetailModelList;

    public DirectorioDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_directorio_detail, container, false);

        init();

        return v;
    }

    private void init() {
        recyclerView = v.findViewById(R.id.rv_directorio_details);
        back = v.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new DirectorioFragment()).addToBackStack(null).commit();
            }
        });

        setModelItems();
        pAdapter = new DirectrioDetailsAdapter(getActivity(), directorioDetailModelList);
        pAdapter.setOnItemClickListener(new DirectrioDetailsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new CivilRecyclerDetailFragment()).addToBackStack(null).commit();
            }
        });


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }

    private void setModelItems() {
        directorioDetailModelList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            DirectorioDetailModel directorioDetailModel = new DirectorioDetailModel();
            directorioDetailModel.setTitle("Cementos Sol");
            directorioDetailModelList.add(directorioDetailModel);
        }
    }
}
