package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtechsoft.tileapp.Models.ImageSliderModel;
import com.mtechsoft.tileapp.Models.VideoSliderModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ImageSliderAdapter;
import com.mtechsoft.tileapp.adapters.VideoSliderAdapter;

import java.util.ArrayList;
import java.util.List;

public class AllProfileFragment extends Fragment {
    public AllProfileFragment() {
        // Required empty public constructor
    }

    private View v;
    ImageView profile_img, back;
    Button invite_friend;
    private RecyclerView recyclerView, recyclerView1;
    private ImageSliderAdapter pAdapter;
    private VideoSliderAdapter mAdapter;
    private List<VideoSliderModel> videoSliderModels;
    private List<ImageSliderModel> imageSliderModels;
    private LinearLayoutManager horizontalLayoutManager;
    TextView dni, fencha, nacionalidad, email, email2, direccion, direccion2, phone, phone2, exp_text;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_all_profile, container, false);

        init();
        return v;
    }

    private void init() {
        profile_img = v.findViewById(R.id.profile_img);
        back = v.findViewById(R.id.back);
        recyclerView = v.findViewById(R.id.image_slider);
        recyclerView1 = v.findViewById(R.id.image_slider1);

        setPoemsName();
        pAdapter = new ImageSliderAdapter(getActivity(), imageSliderModels);
        horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManager);
        recyclerView.setAdapter(pAdapter);

        setImages();
        mAdapter = new VideoSliderAdapter(getActivity(), videoSliderModels);
        horizontalLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView1.setLayoutManager(horizontalLayoutManager);
        recyclerView1.setAdapter(mAdapter);

        invite_friend = v.findViewById(R.id.invite_friend);

        dni = v.findViewById(R.id.dni);
        fencha = v.findViewById(R.id.fencha);
        nacionalidad = v.findViewById(R.id.nacionalidad);
        email = v.findViewById(R.id.email);
        email2 = v.findViewById(R.id.email2);
        direccion = v.findViewById(R.id.direccion);
        direccion2 = v.findViewById(R.id.direccion2);
        phone = v.findViewById(R.id.phone);
        phone2 = v.findViewById(R.id.phone2);
        exp_text = v.findViewById(R.id.exp_text);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new ContractorProfileFragment())
                        .addToBackStack(null).commit();
            }
        });

    }

    private void setPoemsName() {
        imageSliderModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ImageSliderModel imageSliderModel = new ImageSliderModel();
            imageSliderModels.add(imageSliderModel);

        }
    }

    private void setImages() {
        videoSliderModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            VideoSliderModel videoSliderModel = new VideoSliderModel();
            videoSliderModels.add(videoSliderModel);

        }
    }
}
