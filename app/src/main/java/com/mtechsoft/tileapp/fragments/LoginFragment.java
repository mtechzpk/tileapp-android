package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.mtechsoft.tileapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    View view;
    Button bLogin;
    LinearLayout llSignup;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_login, container, false);
        bLogin=view.findViewById(R.id.bLogin);
        llSignup=view.findViewById(R.id.llSignup);
        llSignup.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_loginFragment_to_firstRegisterFragment));
        bLogin.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_loginFragment_to_welcomFragment));
        return  view;
    }
}
