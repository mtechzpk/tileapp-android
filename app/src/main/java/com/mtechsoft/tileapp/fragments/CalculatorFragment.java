package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mtechsoft.tileapp.Models.CalculatorModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.CalculatorAdapter;
import com.mtechsoft.tileapp.fragments.calculator.CeiloResoFragment;
import com.mtechsoft.tileapp.fragments.calculator.ConcretoFragment;
import com.mtechsoft.tileapp.fragments.calculator.MuroFragment;
import com.mtechsoft.tileapp.fragments.calculator.RevestimientosFragment;
import com.mtechsoft.tileapp.fragments.calculator.TabiqueFragment;

import java.util.ArrayList;
import java.util.List;

public class CalculatorFragment extends Fragment {
    public CalculatorFragment() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView recyclerView;
    private CalculatorAdapter pAdapter;
    private List<CalculatorModel> calculatorModels;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_calculator, container, false);
        recyclerView = v.findViewById(R.id.rv_calculator);
        initAdapter();
        return v;
    }

    private void initAdapter() {
//        setPoemsName();
        calculatorModels = new ArrayList<>();

        calculatorModels.add(new CalculatorModel("Revestimientos", R.drawable.revestimientos_ic));
        calculatorModels.add(new CalculatorModel("Tabique Drywall", R.drawable.tabique_ic));
        calculatorModels.add(new CalculatorModel("Cielo Raso", R.drawable.cielo_ic));
        calculatorModels.add(new CalculatorModel("Enchapes", R.drawable.enchap_ic));
        calculatorModels.add(new CalculatorModel("Concreto", R.drawable.concreto_ic));
        calculatorModels.add(new CalculatorModel("Muro", R.drawable.muro_ic));
        calculatorModels.add(new CalculatorModel("Pintura", R.drawable.pintura_ic));
        calculatorModels.add(new CalculatorModel("Techos", R.drawable.tec_ic));
        calculatorModels.add(new CalculatorModel("sdfg", R.drawable.add_more_ic));

        pAdapter = new CalculatorAdapter(getActivity(), calculatorModels);
        pAdapter.setOnItemClickListener(new CalculatorAdapter.OnItemClickListener() {
            @Override
            public void onItemclick(int position) {

                if (position == 0) {
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new RevestimientosFragment()).addToBackStack(null).commit();

                } else if (position == 3) {
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new EnchapesFragment()).addToBackStack(null).commit();

                } else if(position == 2){
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new CeiloResoFragment()).addToBackStack(null).commit();

                }else if (position == 4){
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new ConcretoFragment()).addToBackStack(null).commit();
                }else if (position==5){
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new MuroFragment()).addToBackStack(null).commit();
                }else if (position==1){
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new TabiqueFragment()).addToBackStack(null).commit();
                }else {
                    getFragmentManager().beginTransaction().
                            replace(R.id.nav_host_fragment, new EnchapesFragment()).addToBackStack(null).commit();

                }

            }
        });


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }

//    private void setPoemsName() {
//        calculatorModels = new ArrayList<>();
//        for (int i = 1; i <= 10; i++) {
//            CalculatorModel calculatorModel = new CalculatorModel();
//            calculatorModel.setName("Calculator" + i);
//
//            calculatorModels.add(calculatorModel);
//        }
//    }
}
