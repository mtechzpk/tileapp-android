package com.mtechsoft.tileapp.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.mtechsoft.tileapp.R;

import java.util.Arrays;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;

public class Map1Fragment extends Fragment implements OnMapReadyCallback {
    private View v;
    private GoogleMap mMap;
    LocationManager locationManager;
    private String address;
    SupportMapFragment mapFragment;
    private Double currentLatitude = null;
    AutocompleteSupportFragment autocompleteFragment;
    private Double currentLongitude = null;


    private static final String TAG = "";

    public Map1Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_map1, container, false);
//        init();
        return v;
    }

//    private void init() {
//        autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
//        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ADDRESS);
//        if (fields != null) {
//            autocompleteFragment.setPlaceFields(fields);
//        }
//
//        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
//            @Override
//            public void onPlaceSelected(Place place) {
////                 TODO: Get info about the selected place.
////                Toast.makeText(PropertyLocationActivity.this, "Place: " + place.getName() + ", "
////                + place.getLatLng(), Toast.LENGTH_SHORT).show();
//
//                address = place.getAddress();
//                LatLng latLng = place.getLatLng();
//                currentLatitude = latLng.latitude;
//                currentLongitude = latLng.longitude;
//                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.position(latLng);
//                    markerOptions.title(currentLatitude + " : " + currentLongitude);
//                    mMap.clear();
//                    mMap.addMarker(markerOptions);
//
//                    CameraUpdate camera = CameraUpdateFactory
//                            .newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15);
//                    mMap.animateCamera(camera);
//                } else {
//                    Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onError(Status status) {
//                // TODO: Handle the error.
//                Toast.makeText(getActivity(), "Error: " + status, Toast.LENGTH_SHORT).show();
//            }
//        });
//        mapFragment.getMapAsync(this);
//
//    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (getActivity() != null) {
            mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
                    .findFragmentById(R.id.google_map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
                mMap.setMyLocationEnabled(true);
                if (mMap != null) {
                    mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                        @Override
                        public void onMyLocationChange(Location arg0) {
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(arg0.getLatitude(), arg0.getLongitude()))
                                    .title("It's Me!"));

                        }
                    });
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().isMyLocationButtonEnabled();

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        String provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);

            CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 19);
            mMap.animateCamera(yourLocation);
        }


    }


}
