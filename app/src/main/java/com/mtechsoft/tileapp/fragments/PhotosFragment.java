package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mtechsoft.tileapp.Models.ImageSliderModel;
import com.mtechsoft.tileapp.Models.VideoSliderModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ImageSliderAdapter;
import com.mtechsoft.tileapp.adapters.VideoSliderAdapter;

import java.util.ArrayList;
import java.util.List;

public class PhotosFragment extends Fragment {

    public PhotosFragment() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView recyclerView, rvVideos;
    private LinearLayout add_photo;
    private ImageSliderAdapter pAdapter;
    private VideoSliderAdapter vAdapter;
    private GridLayoutManager gridLayoutManager;
    private List<ImageSliderModel> imageSliderModels;
    private List<VideoSliderModel> videoSliderModels;
    private ImageView back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_photos, container, false);

        init();
        return v;
    }

    private void init() {

//        back = v.findViewById(R.id.back);
        add_photo = v.findViewById(R.id.add_photo);
        recyclerView = v.findViewById(R.id.photos_rv);
        rvVideos = v.findViewById(R.id.rvVideos);

        setImages();
        setVideos();
        pAdapter = new ImageSliderAdapter(getActivity(), imageSliderModels);
        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);


        vAdapter = new VideoSliderAdapter(getActivity(), videoSliderModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rvVideos.setLayoutManager(linearLayoutManager);
        rvVideos.setAdapter(vAdapter);
//
//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,
////                        new CalculatorFragment()).addToBackStack(null).commit();
//            }
//        });
    }

    private void setImages() {
        imageSliderModels = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            ImageSliderModel imageSliderModel = new ImageSliderModel();
            imageSliderModels.add(imageSliderModel);

        }
    }

    private void setVideos() {
        videoSliderModels = new ArrayList<>();
        for (int j = 1; j <= 10; j++) {
            VideoSliderModel videoSliderModel = new VideoSliderModel();
            videoSliderModels.add(videoSliderModel);

        }
    }
}
