package com.mtechsoft.tileapp.fragments.calculator;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.fragments.CalculatorFragment;
import com.mtechsoft.tileapp.fragments.CalculatorResultadoFragment;

public class TabiqueFragment extends Fragment implements View.OnClickListener {

    public TabiqueFragment() {
        // Required empty public constructor
    }

    private View v;
    private LinearLayout dry_linear, dry_linear1, dry_linear2;
    private TextView drywall_text1, drywall_text2, drywall_text3;
    private Button calcular;
    private ImageView back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_tabique, container, false);

        init();
        return v;
    }

    private void init() {
        calcular = v.findViewById(R.id.bLogin);
        calcular.setOnClickListener(this);
        back = v.findViewById(R.id.back);
        back.setOnClickListener(this);

        dry_linear = v.findViewById(R.id.dry_linear);
        dry_linear1 = v.findViewById(R.id.dry_linear1);
        dry_linear2 = v.findViewById(R.id.dry_linear2);
        dry_linear.setOnClickListener(this);
        dry_linear1.setOnClickListener(this);
        dry_linear2.setOnClickListener(this);

        drywall_text1 = v.findViewById(R.id.drywall_text1);
        drywall_text2 = v.findViewById(R.id.drywall_text2);
        drywall_text3 = v.findViewById(R.id.drywall_text3);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bLogin:
                DialogOpen();
                break;
            case R.id.back:
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new CalculatorFragment())
                        .addToBackStack(null).commit();
                break;

            case R.id.dry_linear:
                drywall_text1.setTextColor(getResources().getColor(R.color.blue));
                drywall_text2.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text3.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text1.setTypeface(null, Typeface.BOLD);
                drywall_text2.setTypeface(null, Typeface.NORMAL);
                drywall_text3.setTypeface(null, Typeface.NORMAL);
                break;

            case R.id.dry_linear1:
                drywall_text2.setTextColor(getResources().getColor(R.color.blue));
                drywall_text1.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text3.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text2.setTypeface(null, Typeface.BOLD);
                drywall_text1.setTypeface(null, Typeface.NORMAL);
                drywall_text3.setTypeface(null, Typeface.NORMAL);
                break;

            case R.id.dry_linear2:
                drywall_text3.setTextColor(getResources().getColor(R.color.blue));
                drywall_text2.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text1.setTextColor(getResources().getColor(R.color.colorBlack));
                drywall_text3.setTypeface(null, Typeface.BOLD);
                drywall_text2.setTypeface(null, Typeface.NORMAL);
                drywall_text1.setTypeface(null, Typeface.NORMAL);
                break;
        }
    }

    private void DialogOpen() {
        getFragmentManager().beginTransaction().
                replace(R.id.nav_host_fragment, new CalculatorResultadoFragment()).addToBackStack(null).commit();
    }
}
