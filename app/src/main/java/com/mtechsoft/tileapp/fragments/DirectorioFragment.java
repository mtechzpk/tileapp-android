package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.mtechsoft.tileapp.Models.CalculatorModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.CalculatorAdapter;
import com.mtechsoft.tileapp.adapters.DirectorioAdapter;
import com.mtechsoft.tileapp.fragments.calculator.CeiloResoFragment;
import com.mtechsoft.tileapp.fragments.calculator.ConcretoFragment;
import com.mtechsoft.tileapp.fragments.calculator.MuroFragment;
import com.mtechsoft.tileapp.fragments.calculator.RevestimientosFragment;
import com.mtechsoft.tileapp.fragments.calculator.TabiqueFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DirectorioFragment extends Fragment {
    View view;
    Spinner spCategory;
    private RecyclerView recyclerView;
    private DirectorioAdapter pAdapter;
    private List<CalculatorModel> calculatorModels;

    public DirectorioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_directorio, container, false);
        spCategory = view.findViewById(R.id.spCategory);
        recyclerView = view.findViewById(R.id.rv_calculator);
        List<String> enchapes_list = new ArrayList<String>();
        enchapes_list.add("Materials de Construction");
        enchapes_list.add("etc");
        enchapes_list.add("etc");
        enchapes_list.add("etc");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, enchapes_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(dataAdapter);
        initAdapter();
        return view;
    }

    private void initAdapter() {
//        setPoemsName();
        calculatorModels = new ArrayList<>();

        calculatorModels.add(new CalculatorModel("Cemento Sol", R.drawable.revestimientos_ic));
        calculatorModels.add(new CalculatorModel("Cemento Andino", R.drawable.tabique_ic));
        calculatorModels.add(new CalculatorModel("Cemento inka", R.drawable.cielo_ic));
        calculatorModels.add(new CalculatorModel("Cemento Pacasmayo", R.drawable.enchap_ic));
        calculatorModels.add(new CalculatorModel("Cemento Nacional", R.drawable.concreto_ic));
        calculatorModels.add(new CalculatorModel("Cemento Yura", R.drawable.muro_ic));
        calculatorModels.add(new CalculatorModel("Cemento Sol", R.drawable.revestimientos_ic));
        calculatorModels.add(new CalculatorModel("Cemento Andino", R.drawable.tabique_ic));
        calculatorModels.add(new CalculatorModel("Cemento inka", R.drawable.cielo_ic));
        calculatorModels.add(new CalculatorModel("Cemento Pacasmayo", R.drawable.enchap_ic));
        calculatorModels.add(new CalculatorModel("Cemento Nacional", R.drawable.concreto_ic));
        calculatorModels.add(new CalculatorModel("Cemento Yura", R.drawable.muro_ic));
        pAdapter = new DirectorioAdapter(getActivity(), calculatorModels);
        pAdapter.setOnItemClickListener(new DirectorioAdapter.OnItemClickListener() {
            @Override
            public void onItemclick(int position) {
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new DirectorioDetailFragment()).addToBackStack(null).commit();
            }
        });


        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}
