package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mtechsoft.tileapp.Models.ResultadoModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ResultadoAdapter;

import java.util.ArrayList;

public class CalculatorResultadoFragment extends Fragment {

    public CalculatorResultadoFragment() {
        // Required empty public constructor
    }
    private View v;
    private RecyclerView recyclerView;
    private ResultadoAdapter pAdapter;
    private ArrayList<ResultadoModel> resultadoModels;
    private TextView costo_total,costo_x2, peso_approx;
    RelativeLayout rlDilaog,rvTop;
    Button go_to_map;
    ImageView cross,ivCancel;
    LinearLayout llBottom;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_calculator_resultado, container, false);
        init();
        return v;
    }

    private void init() {
        costo_total = v.findViewById(R.id.costo_total);
        costo_x2 = v.findViewById(R.id.costo_x2);
        peso_approx = v.findViewById(R.id.peso_approx);

        recyclerView = v.findViewById(R.id.rv_resultado);
        rlDilaog = v.findViewById(R.id.rlDilaog);
        ivCancel = v.findViewById(R.id.ivCancel);
        rvTop = v.findViewById(R.id.topRelative);
        go_to_map = v.findViewById(R.id.go_to_map);
        llBottom = v.findViewById(R.id.llBottom);

        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlDilaog.setVisibility(View.GONE);
                llBottom.setVisibility(View.VISIBLE);
                exitToRightAnimation();

            }
        });

        go_to_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlDilaog.setVisibility(View.GONE);
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new Map1Fragment()).addToBackStack(null).commit();
//                        Navigation.createNavigateOnClickListener(R.id.action_contractorProfileFragment_to_allProfileFragment);
            }
        });

        initAdapter();


    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new ResultadoAdapter(getActivity(), resultadoModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);

        pAdapter.setOnItemClickListener(new ResultadoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
//                DialogOpen();
//                rlDilaog.setVisibility(View.VISIBLE);


                if (rlDilaog.getVisibility() == View.VISIBLE) {
                    llBottom.setVisibility(View.VISIBLE);
                    rlDilaog.setVisibility(View.GONE);
                    exitToRightAnimation();
                } else {
                    rlDilaog.setVisibility(View.VISIBLE);
                    llBottom.setVisibility(View.GONE);

                    enterFromRightAnimation();
                }



            }
        });
    }

    private void setPoemsName() {
        resultadoModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            ResultadoModel resultadoModel = new ResultadoModel();
            resultadoModel.setPrice("100" + i);
            resultadoModel.setProduct_name("Pegamento en polvo"+i);
            resultadoModel.setProduct_desc("CELIMA"+i);
            resultadoModel.setProduct_weight("25Kg por bosla");
            resultadoModel.setPage(10);
            resultadoModels.add(resultadoModel);
        }
    }

    private void enterFromRightAnimation() {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.enter_from_right);
        rlDilaog.setVisibility(View.VISIBLE);

        rlDilaog.startAnimation(slideUpAnimation);
    }

    private void exitToRightAnimation() {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.exit_to_right);
        rlDilaog.startAnimation(slideUpAnimation);
    }


    private void DialogOpen() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.calculator_item_view, null);
        dialogBuilder.setView(dialogView);

        ImageView cross = dialogView.findViewById(R.id.cross);
        ImageView item_img = dialogView.findViewById(R.id.item_img);
        TextView item_name = dialogView.findViewById(R.id.item_name);
        TextView item_description = dialogView.findViewById(R.id.item_description);
        TextView presentacion = dialogView.findViewById(R.id.presentacion);
        TextView item_weight = dialogView.findViewById(R.id.item_weight);
        TextView item_price = dialogView.findViewById(R.id.item_price);
        Button go_to_map = dialogView.findViewById(R.id.go_to_map);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

//        Window window = alertDialog.getWindow();
//        WindowManager.LayoutParams wlp = window.getAttributes();
//
//        wlp.gravity = Gravity.END;
////                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//        window.setAttributes(wlp);

        alertDialog.setCanceledOnTouchOutside(false);

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        go_to_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new Map1Fragment()).addToBackStack(null).commit();
//                        Navigation.createNavigateOnClickListener(R.id.action_contractorProfileFragment_to_allProfileFragment);
            }
        });
    }
}
