package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mtechsoft.tileapp.Models.FriendsModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.FriendAdapter;

import java.util.ArrayList;

public class MyFriendFragment extends Fragment {
    View view;
    private RecyclerView recyclerView;
    private FriendAdapter pAdapter;
    private ArrayList<FriendsModel> friendsModels;

    public MyFriendFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_friend, container, false);
        recyclerView = view.findViewById(R.id.rv_poemName);
        initAdapter();
        return view;
    }

    private void setPoemsName() {
        friendsModels = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            FriendsModel friendsModel = new FriendsModel();
            friendsModel.setUserNAme("Steve Jobs" + i);
            friendsModel.setPage(10);
            friendsModels.add(friendsModel);
        }
    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new FriendAdapter(getActivity(), friendsModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }
}
