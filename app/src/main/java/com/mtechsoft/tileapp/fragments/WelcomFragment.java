package com.mtechsoft.tileapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mtechsoft.tileapp.MainActivity;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.activities.LandingActivity;
import com.mtechsoft.tileapp.activities.SplashActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomFragment extends Fragment {
View view;
    Button bCreate;
    public WelcomFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_welcom, container, false);
        initViews();
        onClicks();
        return view;
    }

    private void onClicks() {
        bCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        });
    }

    private void initViews() {
        bCreate=view.findViewById(R.id.bCreate);
    }
}
