package com.mtechsoft.tileapp.fragments.calculator;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.fragments.CalculatorFragment;
import com.mtechsoft.tileapp.fragments.CalculatorResultadoFragment;

import java.util.ArrayList;
import java.util.List;

public class MuroFragment extends Fragment implements View.OnClickListener {
    private View v;
    private Spinner sp_enchapes;
    private LinearLayout brick_linear, brick_linear1, brick_linear2, brick_linear3;
    private TextView brick1_text, brick2_text, brick3_text, brick4_text;
    private Button calcular;
    private ImageView back;

    public MuroFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_muro, container, false);

        init();

        return v;
    }

    private void init() {
        sp_enchapes = v.findViewById(R.id.sp_enchapes);
        calcular = v.findViewById(R.id.bLogin);
        brick_linear = v.findViewById(R.id.brick_linear);
        brick_linear.setOnClickListener(this);
        brick_linear1 = v.findViewById(R.id.brick_linear1);
        brick_linear1.setOnClickListener(this);
        brick_linear2 = v.findViewById(R.id.brick_linear2);
        brick_linear2.setOnClickListener(this);
        brick_linear3 = v.findViewById(R.id.brick_linear3);
        brick_linear3.setOnClickListener(this);

        brick1_text = v.findViewById(R.id.brick1_text);
        brick2_text = v.findViewById(R.id.brick2_text);
        brick3_text = v.findViewById(R.id.brick3_text);
        brick4_text = v.findViewById(R.id.brick4_text);
        back = v.findViewById(R.id.back);
        back.setOnClickListener(this);
        calcular.setOnClickListener(this);

        List<String> enchapes_list = new ArrayList<String>();
        enchapes_list.add("Asentado de ladrillos");
        enchapes_list.add("etc");
        enchapes_list.add("etc");
        enchapes_list.add("etc");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, enchapes_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_enchapes.setAdapter(dataAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.brick_linear:
                brick1_text.setTextColor(getResources().getColor(R.color.blue));
                brick1_text.setTypeface(null, Typeface.BOLD);
                brick2_text.setTypeface(null, Typeface.NORMAL);
                brick3_text.setTypeface(null, Typeface.NORMAL);
                brick4_text.setTypeface(null, Typeface.NORMAL);
                brick2_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick3_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick4_text.setTextColor(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.brick_linear1:
                brick2_text.setTextColor(getResources().getColor(R.color.blue));
                brick2_text.setTypeface(null, Typeface.BOLD);
                brick1_text.setTypeface(null, Typeface.NORMAL);
                brick3_text.setTypeface(null, Typeface.NORMAL);
                brick4_text.setTypeface(null, Typeface.NORMAL);
                brick1_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick3_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick4_text.setTextColor(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.brick_linear2:
                brick3_text.setTextColor(getResources().getColor(R.color.blue));
                brick3_text.setTypeface(null, Typeface.BOLD);
                brick2_text.setTypeface(null, Typeface.NORMAL);
                brick1_text.setTypeface(null, Typeface.NORMAL);
                brick4_text.setTypeface(null, Typeface.NORMAL);
                brick2_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick1_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick4_text.setTextColor(getResources().getColor(R.color.colorBlack));
                break;
            case R.id.brick_linear3:
                brick4_text.setTextColor(getResources().getColor(R.color.blue));
                brick4_text.setTypeface(null, Typeface.BOLD);
                brick2_text.setTypeface(null, Typeface.NORMAL);
                brick3_text.setTypeface(null, Typeface.NORMAL);
                brick1_text.setTypeface(null, Typeface.NORMAL);
                brick2_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick3_text.setTextColor(getResources().getColor(R.color.colorBlack));
                brick1_text.setTextColor(getResources().getColor(R.color.colorBlack));
                break;

            case R.id.bLogin:
                DialogOpen();
                break;

            case R.id.back:
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, new CalculatorFragment())
                        .addToBackStack(null).commit();
                break;
        }
    }

    private void DialogOpen() {
        getFragmentManager().beginTransaction().
                replace(R.id.nav_host_fragment, new CalculatorResultadoFragment()).addToBackStack(null).commit();
    }
}