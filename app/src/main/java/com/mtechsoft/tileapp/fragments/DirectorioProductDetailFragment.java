package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mtechsoft.tileapp.R;

public class DirectorioProductDetailFragment extends Fragment {
    public DirectorioProductDetailFragment() {
        // Required empty public constructor
    }

    private View v;
    private ImageView back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_directorio_product_detail, container, false);

        init();

        return v;
    }

    private void init() {

        back = v.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new DirectorioDetailFragment()).addToBackStack(null).commit();
            }
        });

    }
}
