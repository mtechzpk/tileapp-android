package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mtechsoft.tileapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondRegisterFragment extends Fragment {
View view;
Button bSave;
LinearLayout llProfile;
TextView add_phone;
    EditText phone2, phone;

    public SecondRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_second_register, container, false);
        bSave=view.findViewById(R.id.bSave);
        llProfile=view.findViewById(R.id.llProfile);
        add_phone = view.findViewById(R.id.add_phone);
        phone = view.findViewById(R.id.phone);
        phone2 = view.findViewById(R.id.phone2);

        add_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phone2.setVisibility(View.VISIBLE);
            }
        });

        llProfile.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondRegisterFragment_to_editProfile));
        bSave.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_secondRegisterFragment_to_loginFragment));
        return  view;
    }
}
