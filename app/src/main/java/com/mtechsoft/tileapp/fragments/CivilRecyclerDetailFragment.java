package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mtechsoft.tileapp.Models.ImageModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.SliderViewpagerAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CivilRecyclerDetailFragment extends Fragment {
    ViewPager mPager;
    private ArrayList<ImageModel> imageModelArrayList;
    private int[] myImageList = new int[]{R.drawable.rocks, R.drawable.rocks,
            R.drawable.rocks,R.drawable.rocks};
    int NUM_PAGES = 0;
    int currentPage = 0;
View view;
    ImageView next,previous;
    public CivilRecyclerDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_civil_recycler_detail, container, false);

        next = view.findViewById(R.id.next);
        previous = view.findViewById(R.id.back);

        mPager = (ViewPager) view.findViewById(R.id.pager);
        imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();
        mPager.setAdapter(new SliderViewpagerAdapter(getActivity(),imageModelArrayList));
        NUM_PAGES = myImageList.length;


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentPage = currentPage + 1;
                mPager.setCurrentItem(currentPage);
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentPage == 0){

                }else {

                    currentPage = currentPage - 1;
                    mPager.setCurrentItem(currentPage);
                }

            }
        });



        return view;
    }
    private ArrayList<ImageModel> populateList(){

        ArrayList<ImageModel> list = new ArrayList<>();

        for(int i = 0; i < myImageList.length; i++){
            ImageModel imageModel = new ImageModel();
            imageModel.setImage_drawable(myImageList[i]);
            list.add(imageModel);

        }
        return list;
    }
}
