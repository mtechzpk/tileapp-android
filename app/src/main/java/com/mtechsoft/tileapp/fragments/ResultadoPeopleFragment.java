package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mtechsoft.tileapp.Models.ResultadoModel;
import com.mtechsoft.tileapp.Models.ResultadoPeopleModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ResultadoAdapter;
import com.mtechsoft.tileapp.adapters.ResultadoPeopleAdapter;

import java.util.ArrayList;

public class ResultadoPeopleFragment extends Fragment {
    public ResultadoPeopleFragment() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView recyclerView;
    private ResultadoPeopleAdapter pAdapter;
    private ArrayList<ResultadoPeopleModel> resultadoPeopleModels;
    private ImageView back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_resultado_people, container, false);

        init();
        return v;
    }

    private void init() {
        back = v.findViewById(R.id.back);
        recyclerView = v.findViewById(R.id.rv_resultado_people);

        initAdapter();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,new ServiceOfferedFragment())
                        .addToBackStack(null).commit();
            }
        });

    }

    private void initAdapter() {

        setPoemsName();
        pAdapter = new ResultadoPeopleAdapter(getActivity(), resultadoPeopleModels);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);

        pAdapter.setOnItemClickListener(new ResultadoPeopleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                DialogOpen();
            }
        });
    }

    private void DialogOpen() {
        getFragmentManager().beginTransaction().
                replace(R.id.nav_host_fragment, new ContractorProfileFragment()).addToBackStack(null).commit();
    }

    private void setPoemsName() {
        resultadoPeopleModels = new ArrayList<>();
        for (int i = 1; i <= 9; i++) {
            ResultadoPeopleModel resultadoModel = new ResultadoPeopleModel();
            resultadoModel.setAmigo("Amigo\n" + 100 + i);
            resultadoModel.setName_person("Amigo" + i);
            resultadoModel.setPage(10);
            resultadoPeopleModels.add(resultadoModel);
        }
    }
}
