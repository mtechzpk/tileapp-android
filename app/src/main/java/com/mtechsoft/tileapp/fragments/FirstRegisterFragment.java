package com.mtechsoft.tileapp.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.mtechsoft.tileapp.MainActivity;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.activities.LandingActivity;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class FirstRegisterFragment extends Fragment {
View view;
    TextView tvRegistr, country_name, add_phone;
    Spinner category, district;
    Button bNext;


    public FirstRegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_first_register, container, false);
        category = (Spinner) view.findViewById(R.id.spCategory);
        district = (Spinner) view.findViewById(R.id.spProvince);
        country_name = view.findViewById(R.id.country_name);
        bNext = view.findViewById(R.id.bNext);





        bNext.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_firstRegisterFragment_to_secondRegisterFragment));
        String[] arraySpinner = new String[] {
                "Seleccionar Categoria", "Obrero trabajador independiente", "Constructora"
        };
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, arraySpinner
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        category.setAdapter(spinnerArrayAdapter);

        country_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
                picker.setListener(new CountryPickerListener() {
                    @Override
                    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                        // Implement your code here
                        country_name.setText(name);
                        picker.dismiss();
                    }
                });
                picker.show(getChildFragmentManager(), "COUNTRY_PICKER");
            }
        });

        String[] district_array = new String[]{"District", "District1", "District2"};
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item, district_array
        );
        spinnerArrayAdapter1.setDropDownViewResource(R.layout.spinner_item);
        district.setAdapter(spinnerArrayAdapter1);

        return view;
    }
}
