package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mtechsoft.tileapp.Models.ServiceOfferedModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.ServiceOfferedAdapter;

import java.util.ArrayList;
import java.util.List;

public class ServiceOfferedFragment extends Fragment {

    public ServiceOfferedFragment() {
        // Required empty public constructor
    }

    private View v;
    private RecyclerView recyclerView;
    private ServiceOfferedAdapter pAdapter;
    RelativeLayout rlDilaog;
    ImageView cross;
    private ArrayList<ServiceOfferedModel> serviceOfferedModels;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_service_offered, container, false);

        recyclerView = v.findViewById(R.id.rv_service_offered);
        rlDilaog = v.findViewById(R.id.rlDialog);
        cross = v.findViewById(R.id.cross);
        onClick();
        initAdapter();
        DialogOpen();

        return v;
    }

    private void onClick() {
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlDilaog.setVisibility(View.GONE);
                exitToRightAnimation();
                pAdapter.notifyDataSetChanged();

            }
        });
    }

    private void initAdapter() {

        serviceOfferedModels = new ArrayList<>();

        serviceOfferedModels.add(new ServiceOfferedModel("Albanelirea", R.drawable.service1));
        serviceOfferedModels.add(new ServiceOfferedModel("Electricidad", R.drawable.service2));
        serviceOfferedModels.add(new ServiceOfferedModel("Gasfilteria", R.drawable.service3));
        serviceOfferedModels.add(new ServiceOfferedModel("DryWall", R.drawable.service4));
        serviceOfferedModels.add(new ServiceOfferedModel("Enchapes", R.drawable.service5));
        serviceOfferedModels.add(new ServiceOfferedModel("Vidrios", R.drawable.service6));
        serviceOfferedModels.add(new ServiceOfferedModel("Pintura", R.drawable.service7));
        serviceOfferedModels.add(new ServiceOfferedModel("Prevension", R.drawable.service8));
        serviceOfferedModels.add(new ServiceOfferedModel("Instalacion", R.drawable.service9));
        serviceOfferedModels.add(new ServiceOfferedModel("Soldadura", R.drawable.service10));
        serviceOfferedModels.add(new ServiceOfferedModel("Carpinteria", R.drawable.service11));
        serviceOfferedModels.add(new ServiceOfferedModel("Profesional", R.drawable.service12));
        serviceOfferedModels.add(new ServiceOfferedModel("Letreros", R.drawable.service13));
        serviceOfferedModels.add(new ServiceOfferedModel("Extincion", R.drawable.service14));
        serviceOfferedModels.add(new ServiceOfferedModel("Refregeration", R.drawable.service15));
        serviceOfferedModels.add(new ServiceOfferedModel("Construcora", R.drawable.service16));
        serviceOfferedModels.add(new ServiceOfferedModel("Maquineria", R.drawable.service17));
        serviceOfferedModels.add(new ServiceOfferedModel("Transporte", R.drawable.service18));
        serviceOfferedModels.add(new ServiceOfferedModel(" mghvhg", R.drawable.add_more_ic));

        pAdapter = new ServiceOfferedAdapter(getActivity(), serviceOfferedModels);

        pAdapter.setOnItemClickListener(new ServiceOfferedAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {

                if (rlDilaog.getVisibility() == View.VISIBLE) {
                    rlDilaog.setVisibility(View.GONE);
                    exitToRightAnimation();
                } else {
                    rlDilaog.setVisibility(View.VISIBLE);
                    enterFromRightAnimation();
                }


            }
        });

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(pAdapter);
    }

    private void enterFromRightAnimation() {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.enter_from_right);
        rlDilaog.setVisibility(View.VISIBLE);

        rlDilaog.startAnimation(slideUpAnimation);
    }

    private void exitToRightAnimation() {
        Animation slideUpAnimation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.exit_to_right);
        rlDilaog.startAnimation(slideUpAnimation);
    }

//    private void setPoemsName() {
//        serviceOfferedModels = new ArrayList<>();
//        for (int i = 1; i <= 20; i++) {
//            ServiceOfferedModel serviceOfferedModel = new ServiceOfferedModel();
//            serviceOfferedModel.setService_name("Service " + i);
//            serviceOfferedModels.add(serviceOfferedModel);
//        }
//    }

    public void DialogOpen() {
        ImageView cross = v.findViewById(R.id.cross);
        TextView item_name = v.findViewById(R.id.item_name);
        final Spinner sp_city = v.findViewById(R.id.sp_city);
        final EditText address = v.findViewById(R.id.address);
        final Spinner sp_catagory = v.findViewById(R.id.sp_catagory);
        final Spinner sp_Especialidad = v.findViewById(R.id.sp_Especialidad);
        Button buscar = v.findViewById(R.id.buscar);

        // Spinner Drop down elements
        List<String> cities = new ArrayList<String>();
        cities.add("City 1");
        cities.add("City 2");
        cities.add("City 3");
        cities.add("City 4");
        cities.add("City 5");
        cities.add("City 6");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, cities);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_city.setAdapter(dataAdapter);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Category 1");
        categories.add("Category 2");
        categories.add("Category 3");
        categories.add("Category 4");
        categories.add("Category 5");
        categories.add("Category 6");

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_catagory.setAdapter(dataAdapter2);

        // Spinner Drop down elements
        List<String> especiality = new ArrayList<String>();
        especiality.add("Especialidad 1");
        especiality.add("Especialidad 2");
        especiality.add("Especialidad 3");
        especiality.add("Especialidad 4");
        especiality.add("Especialidad 5");
        especiality.add("Especialidad 6");

        ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, especiality);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_Especialidad.setAdapter(dataAdapter3);


        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String adres = "", city = "", category = "", ecpecialidad = "";
                adres = address.getText().toString();
                category = sp_catagory.getSelectedItem().toString();
                ecpecialidad = sp_Especialidad.getSelectedItem().toString();
                city = sp_city.getSelectedItem().toString();

                rlDilaog.setVisibility(View.GONE);
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new ResultadoPeopleFragment()).addToBackStack(null).commit();

            }
        });
    }
}
