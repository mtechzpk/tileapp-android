package com.mtechsoft.tileapp.fragments.calculator;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.fragments.CalculatorFragment;
import com.mtechsoft.tileapp.fragments.CalculatorResultadoFragment;

import java.util.ArrayList;
import java.util.List;

public class ConcretoFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    public ConcretoFragment() {
        // Required empty public constructor
    }

    private View v;
    private Spinner sp_enchapes;
    private RadioGroup rg_concreto;
    private RadioButton rb_concreto1,rb_concreto2,rb_concreto3;
    private Button calcular;
    private ImageView back;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_concreto, container, false);
        init();

        return v;
    }

    private void init() {

        sp_enchapes = v.findViewById(R.id.sp_enchapes);
        calcular = v.findViewById(R.id.bLogin);
        back = v.findViewById(R.id.back);
        rg_concreto = v.findViewById(R.id.rg_concreto);
        rb_concreto1 = v.findViewById(R.id.rb_concreto1);
        rb_concreto2 = v.findViewById(R.id.rb_concreto2);
        rb_concreto3 = v.findViewById(R.id.rb_concreto3);
        // Spinner click listener
        sp_enchapes.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> enchapes_list = new ArrayList<String>();
        enchapes_list.add("Solado");
        enchapes_list.add("etc");
        enchapes_list.add("etc");
        enchapes_list.add("etc");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, enchapes_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_enchapes.setAdapter(dataAdapter);

        calcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogOpen();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment,new CalculatorFragment())
                        .addToBackStack(null).commit();
            }
        });


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void DialogOpen() {
        getFragmentManager().beginTransaction().
                replace(R.id.nav_host_fragment, new CalculatorResultadoFragment()).addToBackStack(null).commit();
    }
}
