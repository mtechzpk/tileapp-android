package com.mtechsoft.tileapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.mtechsoft.tileapp.Models.JobsModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.adapters.JobsAdapter;
import com.mtechsoft.tileapp.adapters.ResultadoAdapter;

import java.util.ArrayList;
import java.util.List;


public class JobsFragment extends Fragment {

    View view;

    public JobsFragment() {
        // Required empty public constructor
    }

    Spinner sp_enchapes;
    private RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_jobs, container, false);
        sp_enchapes = view.findViewById(R.id.sp_enchapes);
// Spinner Drop down elements
        List<String> enchapes_list = new ArrayList<String>();
        enchapes_list.add("Busca por ciudad y provincia");
        enchapes_list.add("etc");
        enchapes_list.add("etc");
        enchapes_list.add("etc");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, enchapes_list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_enchapes.setAdapter(dataAdapter);



        recyclerView = view.findViewById(R.id.rv_resultado);
        ArrayList<JobsModel> jobsModels = new ArrayList<>();

        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));
        jobsModels.add(new JobsModel("Nombre del trabajo que se ofrece","Rozen social da la me raje nvsc lencto sldjnv jkn"));

        JobsAdapter pAdapter = new JobsAdapter(getActivity(), jobsModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        pAdapter.setOnItemClickListener(new JobsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                getFragmentManager().beginTransaction().
                        replace(R.id.nav_host_fragment, new JobDetailFragment()).addToBackStack(null).commit();
            }
        });

        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(pAdapter);

        return view;
    }
}
