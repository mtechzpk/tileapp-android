package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.ResultadoModel;
import com.mtechsoft.tileapp.R;

import java.util.ArrayList;

public class ResultadoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ResultadoModel> resultadoModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public ResultadoAdapter(Context context, ArrayList<ResultadoModel> resultadoModels) {
        this.context = context;
        this.resultadoModels = resultadoModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_resultado_rv, parent, false);
        return new BookViewHolder(itemView,mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return resultadoModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView product_name, product_desc, product_weight, product_price;
        ImageView product_img;

        private BookViewHolder(View itemView,final OnItemClickListener listener) {
            super(itemView);

            //init views
            product_name = itemView.findViewById(R.id.product_name);
            product_desc = itemView.findViewById(R.id.product_desc);
            product_weight = itemView.findViewById(R.id.product_weight);
            product_price = itemView.findViewById(R.id.product_price);
            product_img = itemView.findViewById(R.id.product_img);
//            country_flag      = itemView.findViewById(R.id.country_flag);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null){
                        int position = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
        }

        private void bind(int pos) {
            ResultadoModel resultadoModel = resultadoModels.get(pos);

            product_name.setText(resultadoModel.getProduct_name());
            product_desc.setText(resultadoModel.getProduct_desc());
            product_weight.setText(resultadoModel.getProduct_weight());
            product_price.setText(resultadoModel.getPrice());
//            initClickListener();
        }
    }
}
