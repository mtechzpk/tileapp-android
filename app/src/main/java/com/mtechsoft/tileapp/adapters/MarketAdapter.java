package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.MarketModel;
import com.mtechsoft.tileapp.R;

import java.util.List;

public class MarketAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<MarketModel> marketModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemclick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public MarketAdapter(Context context, List<MarketModel> marketModels) {
        this.context = context;
        this.marketModels = marketModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_market_place, parent, false);
        return new BookViewHolder(itemView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return marketModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView market_name, market_desc;
        ImageView image_market;
        LinearLayout markte_linear;

        private BookViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            //init views
            market_name = itemView.findViewById(R.id.market_name);
            market_desc = itemView.findViewById(R.id.market_desc);
            image_market = itemView.findViewById(R.id.image_market);
            markte_linear = itemView.findViewById(R.id.markte_linear);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null){
                        int position = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemclick(position);
                        }
                    }

                }
            });

        }

        private void bind(int pos) {
            MarketModel marketModel = marketModels.get(pos);
//            tvname.setText(MarketModel.getName());
//            calculator_img.setImageResource(MarketModel.getImage());

        }
    }
}
