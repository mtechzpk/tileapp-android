package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.JobsModel;
import com.mtechsoft.tileapp.R;
import com.mtechsoft.tileapp.fragments.JobDetailFragment;

import java.util.ArrayList;

public class JobsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
private Context context;
private ArrayList<JobsModel> resultadoModels;
private OnItemClickListener mListener;

public interface OnItemClickListener{
    void onItemClick(int position);
}
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public JobsAdapter(Context context, ArrayList<JobsModel> resultadoModels) {
        this.context = context;
        this.resultadoModels = resultadoModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.job_item, parent, false);
        return new BookViewHolder(itemView,mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return resultadoModels.size();
    }

public class BookViewHolder extends RecyclerView.ViewHolder {

    TextView product_name, product_desc, product_weight, product_price;
    ImageView product_img;

    private BookViewHolder(View itemView,final OnItemClickListener listener) {
        super(itemView);

        //init views
        product_name = itemView.findViewById(R.id.name);
        product_desc = itemView.findViewById(R.id.dec);

//            country_flag      = itemView.findViewById(R.id.country_flag);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener!=null){
                    int position = getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION){
                        listener.onItemClick(position);

                    }
                }

            }
        });
    }

    private void bind(int pos) {
        JobsModel resultadoModel = resultadoModels.get(pos);

        product_name.setText(resultadoModel.getProduct_name());
        product_desc.setText(resultadoModel.getProduct_desc());

//            initClickListener();
    }
}
}