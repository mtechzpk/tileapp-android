package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.FriendsModel;
import com.mtechsoft.tileapp.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<FriendsModel> friendsModels;

    public FriendAdapter(Context context, ArrayList<FriendsModel> friendsModels) {
        this.context = context;
        this.friendsModels = friendsModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_friends, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return friendsModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvUsernaem;
        CircleImageView country_flag;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            tvUsernaem    = itemView.findViewById(R.id.tvUsernaem);
//            country_flag      = itemView.findViewById(R.id.country_flag);
        }

        private void bind(int pos) {
            FriendsModel messagesTabModel = friendsModels.get(pos);
            tvUsernaem.setText(messagesTabModel.getUserNAme());
//            initClickListener();
        }
    }
}
