package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.ResultadoPeopleModel;
import com.mtechsoft.tileapp.R;

import java.util.ArrayList;

public class ResultadoPeopleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ResultadoPeopleModel> resultadoPeopleModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public ResultadoPeopleAdapter(Context context, ArrayList<ResultadoPeopleModel> resultadoPeopleModels) {
        this.context = context;
        this.resultadoPeopleModels = resultadoPeopleModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_resultado_people, parent, false);
        return new BookViewHolder(itemView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return resultadoPeopleModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView amigos, people_name;
        ImageView img_resultado;

        private BookViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            //init views
            amigos = itemView.findViewById(R.id.amigos);
            people_name = itemView.findViewById(R.id.people_name);
            img_resultado = itemView.findViewById(R.id.img_resultado);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null){
                        int position = getAdapterPosition();
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });

        }

        private void bind(int pos) {
            ResultadoPeopleModel ResultadoPeopleModel = resultadoPeopleModels.get(pos);

            amigos.setText(ResultadoPeopleModel.getAmigo());
            people_name.setText(ResultadoPeopleModel.getName_person());
//            initClickListener();
        }
    }
}
