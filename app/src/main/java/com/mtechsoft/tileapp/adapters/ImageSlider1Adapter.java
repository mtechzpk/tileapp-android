package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.ImageSliderModel;
import com.mtechsoft.tileapp.R;

import java.util.List;

public class ImageSlider1Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<ImageSliderModel> imageSliderModels;
//    private OnItemClickListener mListener;

//    public interface OnItemClickListener{
//        void onItemclick(int position);
//    }
//    public void setOnItemClickListener(OnItemClickListener listener){
//        mListener = listener;
//    }

    public ImageSlider1Adapter(Context context, List<ImageSliderModel> imageSliderModels) {
        this.context = context;
        this.imageSliderModels = imageSliderModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_image_slider, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return imageSliderModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {


        ImageView image_slider_img;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            image_slider_img    = itemView.findViewById(R.id.image_slider_img);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listener!=null){
//                        int position = getAdapterPosition();
//                        if (position!=RecyclerView.NO_POSITION){
//                            listener.onItemclick(position);
//                        }
//                    }
//
//                }
//            });

        }

        private void bind(int pos) {
            ImageSliderModel imageSliderModel = imageSliderModels.get(pos);
//            image_slider_img.setImageDrawable(imageSliderModel.getImage());

//            initClickListener();
        }
    }
}

