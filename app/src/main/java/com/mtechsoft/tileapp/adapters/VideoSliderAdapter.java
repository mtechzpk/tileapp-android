package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.VideoSliderModel;
import com.mtechsoft.tileapp.R;

import java.util.List;

public class VideoSliderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<VideoSliderModel> videoSliderModels;
//    private OnItemClickListener mListener;

//    public interface OnItemClickListener{
//        void onItemclick(int position);
//    }
//    public void setOnItemClickListener(OnItemClickListener listener){
//        mListener = listener;
//    }

    public VideoSliderAdapter(Context context, List<VideoSliderModel> videoSliderModels) {
        this.context = context;
        this.videoSliderModels = videoSliderModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_video_slider, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return videoSliderModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {


        ImageView img_view;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            img_view    = itemView.findViewById(R.id.img_view);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (listener!=null){
//                        int position = getAdapterPosition();
//                        if (position!=RecyclerView.NO_POSITION){
//                            listener.onItemclick(position);
//                        }
//                    }
//
//                }
//            });

        }

        private void bind(int pos) {
            VideoSliderModel videoSliderModel = videoSliderModels.get(pos);
//            image_slider_img.setImageDrawable(videoSliderModel.getImage());

//            initClickListener();
        }
    }
}
