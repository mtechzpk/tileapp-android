package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.ServiceOfferedModel;
import com.mtechsoft.tileapp.R;

import java.util.ArrayList;

public class ServiceOfferedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ServiceOfferedModel> serviceOfferedModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public ServiceOfferedAdapter(Context context, ArrayList<ServiceOfferedModel> calculatorModels) {
        this.context = context;
        this.serviceOfferedModels = calculatorModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_service_offered, parent, false);
        return new BookViewHolder(itemView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return serviceOfferedModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView service_name;
        ImageView imageView;
        LinearLayout linearLayout;

        private BookViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            //init views
            service_name    = itemView.findViewById(R.id.service_name);
            imageView    = itemView.findViewById(R.id.img_service_offered);
            linearLayout    = itemView.findViewById(R.id.linear);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null){
                        int position = getAdapterPosition();
                        linearLayout.setBackgroundColor(Color.parseColor("#0276DA"));
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);

//                            notifyDataSetChanged();
                        }
                    }

                }
            });
        }

        private void bind(int pos) {
            ServiceOfferedModel serviceOfferedModel = serviceOfferedModels.get(pos);
            service_name.setText(serviceOfferedModel.getService_name());
            imageView.setImageResource(serviceOfferedModel.getImage());
//            linearLayout.setBackground(R.drawable.gray_border);

            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                linearLayout.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.gray_border) );
            } else {
                linearLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.gray_border));
            }



            if (pos==serviceOfferedModels.size() - 1){
                service_name.setVisibility(View.GONE);
                imageView.requestLayout();
                imageView.getLayoutParams().height = 225;
                imageView.getLayoutParams().width = 155;

            }
//            initClickListener();
        }
    }
}
