package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.CalculatorModel;
import com.mtechsoft.tileapp.R;

import java.util.List;


public class DirectorioAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<CalculatorModel> calculatorModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemclick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DirectorioAdapter(Context context, List<CalculatorModel> calculatorModels) {
        this.context = context;
        this.calculatorModels = calculatorModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_directorio, parent, false);
        return new BookViewHolder(itemView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return calculatorModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvname;
        ImageView calculator_img;
        LinearLayout linearLayout;

        private BookViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            //init views
            tvname = itemView.findViewById(R.id.calculator_text);
            calculator_img = itemView.findViewById(R.id.calculator_img);
            linearLayout = itemView.findViewById(R.id.linearlay);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener!=null){
                        int position = getAdapterPosition();
//                        linearLayout.setBackgroundColor(Color.parseColor("#0276DA"));
                        if (position!=RecyclerView.NO_POSITION){
                            listener.onItemclick(position);
                        }
                    }

                }
            });

        }

        private void bind(int pos) {
            CalculatorModel calculatorModel = calculatorModels.get(pos);
            tvname.setText(calculatorModel.getName());
            calculator_img.setImageResource(calculatorModel.getImage());

//            initClickListener();
        }
    }
}



