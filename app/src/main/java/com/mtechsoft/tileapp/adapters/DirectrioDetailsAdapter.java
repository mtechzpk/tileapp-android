package com.mtechsoft.tileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mtechsoft.tileapp.Models.DirectorioDetailModel;
import com.mtechsoft.tileapp.R;

import java.util.ArrayList;
import java.util.List;

public class DirectrioDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<DirectorioDetailModel> directorioDetailModels;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public DirectrioDetailsAdapter(Context context, List<DirectorioDetailModel> directorioDetailModels) {
        this.context = context;
        this.directorioDetailModels = directorioDetailModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_directrio_details, parent, false);
        return new BookViewHolder(itemView, mListener);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);

    }

    @Override
    public int getItemCount() {
        return directorioDetailModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView image;

        private BookViewHolder(View itemView, final OnItemClickListener listener) {
            super(itemView);

            //init views
            title = itemView.findViewById(R.id.title);
            image = itemView.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);

                        }
                    }

                }
            });
        }

        private void bind(int pos) {
            DirectorioDetailModel resultadoModel = directorioDetailModels.get(pos);
            title.setText(resultadoModel.getTitle());

        }
    }
}
