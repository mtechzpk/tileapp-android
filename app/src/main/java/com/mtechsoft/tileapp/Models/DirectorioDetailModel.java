package com.mtechsoft.tileapp.Models;

public class DirectorioDetailModel {
    String Image;
    String title;

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
