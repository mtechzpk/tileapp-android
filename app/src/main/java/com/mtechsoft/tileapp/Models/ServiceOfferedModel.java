package com.mtechsoft.tileapp.Models;

public class ServiceOfferedModel {

    public ServiceOfferedModel(String service_name, int image) {
        this.service_name = service_name;
        this.image = image;
    }

    String service_name = "";
    int image = 0;

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
