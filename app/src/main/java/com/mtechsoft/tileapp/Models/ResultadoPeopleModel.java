package com.mtechsoft.tileapp.Models;

public class ResultadoPeopleModel {
    String person_img = "";
    String amigo = "";
    int page = 0;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    String name_person = "";

    public String getPerson_img() {
        return person_img;
    }

    public void setPerson_img(String person_img) {
        this.person_img = person_img;
    }

    public String getAmigo() {
        return amigo;
    }

    public void setAmigo(String amigo) {
        this.amigo = amigo;
    }

    public String getName_person() {
        return name_person;
    }

    public void setName_person(String name_person) {
        this.name_person = name_person;
    }
}
