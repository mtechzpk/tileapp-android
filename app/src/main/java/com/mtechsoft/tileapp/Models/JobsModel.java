package com.mtechsoft.tileapp.Models;

public class JobsModel {

    String product_name = "";
    String product_desc = "";

    public JobsModel(String product_name, String product_desc) {
        this.product_name = product_name;
        this.product_desc = product_desc;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_desc() {
        return product_desc;
    }
}
