package com.mtechsoft.tileapp.Models;

public class MarketModel {
    int market_img;
    String market_name="";
    String market_desc="";

    public int getMarket_img() {
        return market_img;
    }

    public void setMarket_img(int market_img) {
        this.market_img = market_img;
    }

    public String getMarket_name() {
        return market_name;
    }

    public void setMarket_name(String market_name) {
        this.market_name = market_name;
    }

    public String getMarket_desc() {
        return market_desc;
    }

    public void setMarket_desc(String market_desc) {
        this.market_desc = market_desc;
    }
}
