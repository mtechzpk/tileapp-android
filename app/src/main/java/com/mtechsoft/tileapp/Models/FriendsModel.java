package com.mtechsoft.tileapp.Models;

public class FriendsModel {
    String userNAme = "";
    int page;
    String countryFlag = "";

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getUserNAme() {
        return userNAme;
    }

    public void setUserNAme(String userNAme) {
        this.userNAme = userNAme;
    }
}
