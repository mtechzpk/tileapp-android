package com.mtechsoft.tileapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class MainActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer, ivDrawer1;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar, rlHome, rlToolbar1;
    TextView tvTitle;
    SearchView svSearch;
    ImageView ivlogo, ivBackGround, ivLogo1;
    FrameLayout frameLayout;
    BottomNavigationView bottomNavigationView;
    private final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rlToolbar = findViewById(R.id.rlToolbar);
//        tvTitle = findViewById(R.id.tvTitle);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.ivDrawer);
        ivlogo = findViewById(R.id.ivlogo);
//        tvTitle = findViewById(R.id.tvTitle);
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);


        //Bottom Navigation hover up with keyboard Solution Code
        KeyboardVisibilityEvent.setEventListener(
                this,
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        Log.d(TAG,"onVisibilityChanged: Keyboard visibility changed");
                        if(isOpen){
                            Log.d(TAG, "onVisibilityChanged: Keyboard is open");
                            bottomNavigationView.setVisibility(View.INVISIBLE);
                            Log.d(TAG, "onVisibilityChanged: NavBar got Invisible");
                        }else{
                            Log.d(TAG, "onVisibilityChanged: Keyboard is closed");
                            bottomNavigationView.setVisibility(View.VISIBLE);
                            Log.d(TAG, "onVisibilityChanged: NavBar got Visible");
                        }
                    }
                });

//        bottomNavigationView.setSelectedItemId(R.id.calculatorFragment);

//        HomeFragment homeFragment=new HomeFragment();
//        replaceFragment(homeFragment,false,false);
//        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        initNavigation();
//        setColor();
    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
//                    if (destination.getLabel().equals("fragment_photos")||destination.getLabel().equals("fragment_directorio_detail")||destination.getLabel().equals("fragment_directorio")||destination.getLabel().equals("fragment_civil_recycler_detail")) {
                    if (destination.getLabel().equals("fragment_service_offered")||destination.getLabel().equals("fragment_my_friend")||destination.getLabel().equals("fragment_calculator")||destination.getLabel().equals("fragment_jobs")||destination.getLabel().equals("fragment_market_place")) {
                        rlToolbar.setVisibility(View.VISIBLE);
                        bottomNavigationView.setVisibility(View.VISIBLE);
                    }
                    else {
                        rlToolbar.setVisibility(View.VISIBLE);
                        bottomNavigationView.setVisibility(View.GONE);
                    }
                }
//                navigationView.getMenu().findItem(R.id.photosFragment).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                        startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
//                        return true;
//                    }
//                });

            }
        });

        navigationView.getMenu().getItem(0).setActionView(R.layout.menu_text);
        navigationView.getMenu().getItem(1).setActionView(R.layout.menu_txt1);
        navigationView.getMenu().getItem(4).setActionView(R.layout.menu_txt1);
        navigationView.getMenu().getItem(5).setActionView(R.layout.menu_txt1);
        navigationView.getMenu().getItem(8).setActionView(R.layout.menu_txt1);

    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawers();

        else if (!navController.getCurrentDestination().getLabel().toString().equals("homeFragment")) {

            super.onBackPressed();
        } else {

            showCustomDialog1();
        }
    }


    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(MainActivity.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }


    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.activity_main2_drawer, menu);

        // change color for icon 0
        Drawable yourdrawable = menu.getItem(0).getIcon(); // change 0 with 1,2 ...
        yourdrawable.mutate();
        yourdrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        return true;
    }
}
